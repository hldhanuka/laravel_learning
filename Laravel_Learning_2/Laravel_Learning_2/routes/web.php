<?php

use App\Models\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/contact', function () {
//     return view('contact');
// });

// Route::resource("posts", "App\Http\Controllers\PostsController");

// ----------- Laravel database -----------
// Route::get('/insert', function () {
//     DB::insert('insert into posts(title,body) values(?,?)', ['software developer', 'himanshu is a software developer']);
// });

// Route::get('/select', function () {
//     $results = DB::select('select * from posts where id=?', [1]);

//     foreach ($results as $posts) {
//         echo "title is :" . $posts->title;
//         echo "<br>";
//         echo "body is:" . $posts->body;
//     }
// });

// Route::get('/update', function () {
//     $updated = DB::update('update posts set title="software tester" where id=?', [1]);

//     return $updated;
// });

// Route::get('/delete', function () {
//     $deleted = DB::delete('delete from posts where id=?', [1]);

//     return $deleted;
// });

// Route::get('/read', function () {
//     $posts = Post::all();

//     foreach ($posts as $post) {
//         echo $post->body;
//         echo '<br>';
//     }
// });

// Route::get('/find', function () {
//     $posts = Post::find(1);

//     return $posts->title;
// });

// Route::get('/find', function () {
//     $posts = Post::where('id', 2)->first();

//     return $posts;
// });

// Route::get('/find', function () {
//     $posts = Post::where('id', 1)->value('title');

//     return $posts;
// });

// Route::get('/insert', function () {
//     $post = new Post;

//     $post->title = 'Nishka';
//     $post->body = 'QA Analyst';

//     $post->save();
// });

// Route::get('/basicupdate', function () {
//     $post = Post::find(2);

//     $post->title = 'Haseena';
//     $post->body = 'Graphic Designer';

//     $post->save();
// });

// Route::get('/create', function () {
//     Post::create(['title' => 'Harshita', 'body' => 'Technical Content Writer']);
// });

// Route::get('/update', function () {
//     Post::where('id', 1)->update(['title' => 'Charu', 'body' => 'technical Content Writer']);
// });

// Route::get('/delete', function () {
//     $post = Post::find(1);

//     $post->delete();
// });

Route::get('/softdelete', function () {
    Post::find(1)->delete();
});

Route::get('/readsofdelete', function () {
    $post = Post::withTrashed()->where('id', 1)->get();

    return $post;
});

Route::get('/restore', function () {
    Post::withTrashed()->where('id', 1)->restore();
});

Route::get('/forcedelete', function () {
    Post::onlyTrashed()->forceDelete();
});
    
// ----------- Laravel database -----------
