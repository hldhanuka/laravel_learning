<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('product1', 'App\Http\Controllers\ProductController@index1');
Route::get('product2', 'App\Http\Controllers\ProductController@index2');
Route::get('product3', 'App\Http\Controllers\ProductController@index3');
Route::get('product4', 'App\Http\Controllers\ProductController@index4');
