<?php

namespace App\Http\Controllers;

use App\Models\Product;

class ProductController extends Controller
{
    public function index1()
    {
        $products = Product::get();

        return view('products1', compact('products'));
    }

    public function index2()
    {
        $products = [];

        return view('products2', compact('products'));
    }

    public function index3()
    {
        $products = [];

        return view('products3', compact('products'));
    }

    public function index4()
    {
        $products = Product::get();

        return view('products4', compact('products'));
    }
}
