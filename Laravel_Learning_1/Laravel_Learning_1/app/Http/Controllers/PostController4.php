<?php

namespace App\Http\Controllers;

class PostController4 extends Controller
{
    public function display()
    {
        return view('student5');
    }

    public function display2()
    {
        return view('student6', ['students' => ['anisha', 'haseena', 'akshita', 'jyotika']]);
    }

    public function display3($id)
    {
        return view('student7')->with('id', $id);
    }
}
