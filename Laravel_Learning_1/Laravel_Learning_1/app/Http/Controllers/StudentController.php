<?php

namespace App\Http\Controllers;

class StudentController extends Controller
{
    public function display()
    {
        return view('student2', ['name1' => 'Anisha', 'name2' => 'Nishka', 'name3' => 'Sumit']);
    }

    public function display2($id)
    {
        return view('student3')->with('id', $id);
    }

    public function display3($name)
    {
        return view('student4', compact('name'));
    }
}
