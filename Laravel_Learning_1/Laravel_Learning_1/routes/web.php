<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/post/{id}', function($id)
// {
//     return "id number is : ". $id;
// });

// Route::get('user/{name?}', function($name=null) {
//     return $name;
// });

// // ------------- Regular Expression Constraints ---------------
// Route::get('user-lastname/{lastname?}', function ($lastname=null) {
//     return $lastname;
// })->where(
//     'lastname','[a-zA-Z]+'
// );

// Route::get('user-id-name/{id}/{name}', function ($id,$name) {
//     return "id is : ". $id ." ,".  "Name is : ".$name ;
// })->where(
//     [
//         'id'=>'[0-9]+',
//         'name'=>'[a-zA-Z]+'
//     ]
// );
// // ------------- Regular Expression Constraints ---------------

// // ------------- Global Constraints TEST ---------------
// Route::get('user/{id}', function ($id) {
//     return $id;
// });

// Route::get('post/{id}', function ($id) {
//     return $id;
// });
// // ------------- Global Constraints TEST ---------------

// ------------- Named Routes ---------------
// Route::get('student/details', 'studentcontroller@showdetails')
//     ->name('student_details');

// Route::get('student/details/example', array(
//     'as'=>'student.details',function()
//     {
//         $url=route('student.details');

//         return "The url is : " .$url;
//     }
// ));

// Route::get('user/{id}/profile', function($id)
// {
//     $url=route('profile', ['id'=>$id]);

//     return $url;  
// })->name('profile');
// ------------- Named Routes ---------------

// // ------------- Navigating from one route to another using named routes ---------------
// Route::Get('/',function()
// {
//     return view('student');
// });

// Route::get('student/details',function()
// {
//     $url=route('student.details');

//     return $url;
// })->name('student.details');
// // ------------- Navigating from one route to another using named routes ---------------

// // ------------- Apply a Middleware -------------

// // Route::Get('/',function()
// // {  
// //     return view('welcome');
// // })->middleware('age');

// Route::Get('/{age}',function()
// {  
//     return view('welcome');
// })->middleware('age');

// Route::Get('user/profile',function()
// {
//     return "user profile";
// });
// // ------------- Apply a Middleware -------------

// ------------- Route Groups -------------
// Route::group([''], function()
// {  
//     Route::get('/first',function()
//     {
//         echo "first route";
//     }
// );

// Route::get('/second',function()
//     {
//         echo "second route";
//     }
// );

// Route::get('/third',function()
//         {
//             echo "third route";
//         }
//     );
// });

// // ---------------
// Route::group(['prefix' => 'tutorial'], function()
// {
//     Route::get('/aws',function()
//     {
//         echo "aws tutorial";
//     }
// )->middleware('age');

// Route::get('/jira',function()
//     {
//         echo "jira tutorial";
//     }
// );

// Route::get('/testng',function()
//     {
//         echo "testng tutorial";
//     });
// });
// // ---------------

// // ---------------
// Route::middleware(['age'])->group( function()
// {
//     Route::get('/aws',function()
//         {
//             echo "aws tutorial";
//         }
//     );

//     Route::get('/jira',function()
//         {
//             echo "jira tutorial";
//         }
//     );

//     Route::get('/testng',function()
//         {
//             echo "testng tutorial";
//         }
//     );
// });
// ---------------

// // ---------------
// Route::name('admin-')->group(function()
// {
//     Route::get('users', function()
//         {
//             echo "test";
//         }
//     )->name('users');
// });
// // ---------------
// ------------- Route Groups -------------

// ------------- Routing Controllers -------------
// Route::get('/post','App\Http\Controllers\PostController@index');
// Route::get('/post/{id}','App\Http\Controllers\PostController@index');
// Route::get('/post/{id}','App\Http\Controllers\PostController');
// Route::resource('posts','App\Http\Controllers\PostController2');
// Route::resource('posts', 'App\Http\Controllers\PostController2', ['only' => ['create', 'show']]);
// Route::resource('posts', 'App\Http\Controllers\PostController2', ['names' => ['index' => 'test']]);
// Route::resource('posts', 'App\Http\Controllers\PostController2', ['parameters' => ['student' => 'id']]);
// ------------- Routing Controllers -------------

// ------------- Controller Middleware -------------
// Route::get('posts', 'App\Http\Controllers\PostController2@create')->middleware('check');
// Route::get('posts', 'App\Http\Controllers\PostController2@index');
// ------------- Controller Middleware -------------

// ------------- The Views -------------
// Route::get('/contact', function () {
//     return view('contact', ['name' => 'John']);
// });

// Route::get('/details', 'App\Http\Controllers\PostController3@display');

// Route::get('/details', 'App\Http\Controllers\StudentController@display');
// Route::get('/details2/{id}', 'App\Http\Controllers\StudentController@display2');
// Route::get('/details3/{name}', 'App\Http\Controllers\StudentController@display3');
// ------------- The Views -------------

// ------------- Blade Template -------------
// Route::get('/details', 'App\Http\Controllers\PostController4@display');
// Route::get('/details2', 'App\Http\Controllers\PostController4@display2');
// Route::get('/details3/{id}', 'App\Http\Controllers\PostController4@display3');
// ------------- Blade Template -------------

// ------------- Template Inheritance -------------
// Route::get('/contact', function () {
//     return view('contact2');
// }); 

// Route::get('/contact', function () {
//     return view('contact3');
// }); 
// ------------- Template Inheritance -------------