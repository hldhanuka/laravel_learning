<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/insert', function () {
    return view('create');
});

Route::resource('users', 'App\Http\Controllers\CrudsController');

Route::resource('student', 'App\Http\Controllers\StudentController');

Route::resource('forms', 'App\Http\Controllers\FormController');

Route::resource('forms2', 'App\Http\Controllers\Form2Controller');

Route::get('/file-upload', function () {
    return view('form');
});

Route::get('/session', function () {
    return view('form');
});
