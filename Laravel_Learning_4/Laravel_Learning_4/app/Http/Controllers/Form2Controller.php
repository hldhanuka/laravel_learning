<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;

class Form2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form2');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     $request->session()->put('user', $request->input('username'));

    //     echo $request->session()->get('user');
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     session(['user' => $request->input('username')]);

    //     $data = session('user');

    //     echo $data;
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // // --------------------- Storing the data in a session
        // $request->session()->put('user', $request->input('username'));

        // echo $request->session()->get('user');
        // // --------------------- Storing the data in a session

        // // --------------------- Global Session Helper
        // session(['user' => $request->input('username')]);

        // $data = session('user');

        // echo $data;
        // // --------------------- Global Session Helper

        // --------------------- Retrieving all session data
        // session(['user1' => 'anushka']);
        // session(['user2' => 'anvi']);

        // return $request->session()->all();
        // --------------------- Retrieving all session data

        // // --------------------- Deleting the session
        // session(['user1' => 'anushka']);
        // session(['user2' => 'anvi']);

        // $request->session()->forget('user1');

        // return $request->session()->all();
        // // --------------------- Deleting the session

        // // --------------------- Removing the session
        // session(['user1' => 'anushka']);
        // session(['user2' => 'anvi']);

        // $request->session()->flush();

        // return $request->session()->all();
        // // --------------------- Removing the session

        // --------------------- Flashing data
        session()->flash('post', 'post has been updated');

        return $request->session()->get('post');
        // --------------------- Flashing data
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
