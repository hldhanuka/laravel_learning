<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    // use SoftDeletes;

    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $fillable =
    [
        'title',
        'body'
    ];
    // protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function photos()
    {
        return $this->morphMany('App\Models\Photo', 'imageable');
    }

    // get all the tags from this post.  
    public function tags()
    {
        return $this->morphToMany('App\Models\Tag', 'taggable');
    }
}
