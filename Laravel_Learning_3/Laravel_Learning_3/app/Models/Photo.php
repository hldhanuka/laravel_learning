<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;

    public function imageable()
    {
        return $this->morphTo();
    }

    // get all the tags from this post.  
    public function tags()
    {
        return $this->morphToMany('App\Models\Tag', 'taggable');
    }
}
