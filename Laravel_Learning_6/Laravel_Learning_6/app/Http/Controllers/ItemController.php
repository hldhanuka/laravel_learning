<?php

namespace App\Http\Controllers;

// use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class ItemController extends Controller
{

    /**  
     * It will display application dashboard.  
     *  
     * It will return \Illuminate\Http\Response  
     */
    public function pdfview(Request $request)
    {
        $items = DB::table("items")->get();

        view()->share('items', $items);

        if ($request->has('download')) {
            $pdf = PDF::loadView('pdfview');

            return $pdf->download('pdfview.pdf');
        }

        return view('pdfview');
    }
}
