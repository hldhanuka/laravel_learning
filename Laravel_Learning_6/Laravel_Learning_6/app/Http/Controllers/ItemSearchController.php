<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;

class ItemSearchController extends Controller
{
    /**
     * It will Display the resource list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = null;

        if ($request->has('search')) {
            $items = Item::search($request->input('search'))->toArray();
        }

        return view('itemSearch', compact('items'));
    }

    /**
     * It will Display the resource list.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $item = Item::create($request->all());

        $item->addToIndex();

        return redirect()->back();
    }
}
