<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('pdfview', array('as' => 'pdfview', 'uses' => 'App\Http\Controllers\ItemController@pdfview'));

Route::post('formSubmit', 'App\Http\Controllers\ImageController@formSubmit');

Route::get('itemSearch', 'App\Http\Controllers\ItemSearchController@index');
Route::post('itemSearchCreate', 'App\Http\Controllers\ItemSearchController@create');
