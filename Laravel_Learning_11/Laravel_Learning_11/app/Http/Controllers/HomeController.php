<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function mail()
    {
        $user = User::find(1)->toArray();

        Mail::send('emails.mailEvent', $user, function ($message) use ($user) {
            $message->to($user['email']);
            $message->subject('Mailgun Testing');
        });

        dd('Mail is Successfully Send');
    }
}
