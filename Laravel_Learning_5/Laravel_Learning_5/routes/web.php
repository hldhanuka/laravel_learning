<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/welcome2', function () {
    return view('welcome2');
});

Route::get('myproducts', 'App\Http\Controllers\ProductController@index');
Route::delete('myproducts/{id}', 'App\Http\Controllers\ProductController@destroy');
Route::delete('myproductsDeleteAll', 'App\Http\Controllers\ProductController@deleteAll');
