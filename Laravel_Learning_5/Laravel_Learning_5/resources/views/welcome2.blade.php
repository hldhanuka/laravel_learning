<!DOCTYPE html>
<html>

<head>
    <title> Use of font awesome </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
    <style type="text/css">
        i {
            font-size: 50px !important;
            padding: 10px;
        }
    </style>
</head>

<body>

    <h1> Use of font awesome </h1>

    <i class="fa fa-copy"></i>
    <i class="fa fa-save"></i>
    <i class="fa fa-trash"></i>
    <i class="fa fa-home"></i>

</body>

</html>