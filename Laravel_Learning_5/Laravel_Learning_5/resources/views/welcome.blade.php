<!DOCTYPE html>
<html>

<head>
    <title> Use of font awesome </title>

    <link type="text /css" rel="stylesheet" href="{{ mix('css/app.css') }}">
    <style type="text /css">
        i {
        font-size: 50px !important;
        padding: 10px;
        }
    </style>
</head>

<body>

    <h1> Use of font awesome </h1>

    <i class="fa fa-copy"></i>
    <i class="fa fa-save"></i>
    <i class="fa fa-trash"></i>
    <i class="fa fa-home"></i>

</body>

</html>