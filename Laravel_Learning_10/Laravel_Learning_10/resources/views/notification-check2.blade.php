<!DOCTYPE html>
<html>

<head>
    <title> Notification message popup using toastr JS Plugin in Laravel</title>
    <script src="http://demo.javatpoint.com/plugin/jquery.js"></script>
    <link rel="stylesheet" href="http://demo.javatpoint.com/plugin/bootstrap-3.min.css">
</head>

<body>
    @include('notification2')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div class="panel-body">
                        Check for notification
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>