<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class MyFirstNotification extends Notification
{
    use Queueable;

    private $details;

    /**  
     * It is used to create a new notification instance.  
     *  
     * @return void  
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**  
     * It is used to get delivery channels of notification.  
     *  
     * @param  mixed  $notifiable  
     * @return array  
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**  
     * It is used to get notification?s mail representation.  
     *  
     * @param  mixed  $notifiable  
     * @return \Illuminate\Notifications\Messages\MailMessage  
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting($this->details['greeting'])
            ->line($this->details['body'])
            ->action($this->details['actionText'], $this->details['actionURL'])
            ->line($this->details['thanks']);
    }

    /**  
     * It is used to get notification?s array representation.  
     *  
     * @param  mixed  $notifiable  
     * @return array  
     */
    public function toDatabase($notifiable)
    {
        return [
            'order_id' => $this->details['order_id']
        ];
    }
}
