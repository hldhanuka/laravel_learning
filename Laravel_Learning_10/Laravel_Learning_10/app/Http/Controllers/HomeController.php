<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\MyFirstNotification;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function sendNotification()
    {
        $user = User::first();

        $details = [
            'greeting' => 'Hi Artisan',
            'body' => 'This is my first notification from Javatpoint.com',
            'thanks' => 'Thank you for using Javatpoint.com tutorial!',
            'actionText' => 'View My Site',
            'actionURL' => url('/'),
            'order_id' => 101
        ];

        Notification::send($user, new MyFirstNotification($details));

        dd('done');
    }

    /**  
     * Show the application dashboard.  
     *  
     * @return \Illuminate\Http\Response  
     */
    public function notification()
    {
        session()->put('success', 'Item is successfully created.');

        return view('notification-check');
    }

    public function notification2()
    {
        session()->put('success', 'Item is successfully created.');

        return view('notification-check2');
    }
}
